<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/news', function () {
    $news = [
        ['id' => 0, 'title' => 'News project', 'description' => 'This news about project cbdhihssdibchdb cbhdkb cbwscgb jwhcvb cebwicgbf wcbfgbefiw ewbh ebjhwjf ', 'data' => '10.03.2022'],
        ['id' => 1, 'title' => 'News design', 'description' => 'This news about design cbdhihssdibchdb cbhdkb cbwscgb jwhcvb cebwicgbf wcbfgbefiw ewbh ebjhwjf ', 'data' => '22.07.2022'],
        ['id' => 2, 'title' => 'News team', 'description' => 'This news about team cbdhihssdibchdb cbhdkb cbwscgb jwhcvb cebwicgbf wcbfgbefiw ewbh ebjhwjf ', 'data' => '18.11.2022']
    ];
    return view('news', ['news' => $news]);
});

Route::get('/news/{newsId}', function ($newsId) {
    $news = [
        ['id' => 0, 'img'=>'https://cdn-icons-png.flaticon.com/512/325/325717.png','title' => 'News project', 'description' => 'This news about project cbibw wbckew wkbvc wbkcjw wbk bwkbefj wkfh bfwkfb kefb nlwefl wkehf ewfnkj dcbb ', 'data' => '10.03.2022'],
        ['id' => 1, 'img'=>'https://cdn-icons-png.flaticon.com/512/325/325714.png','title' => 'News design', 'description' => 'This news about design cbibw wbckew wkbvc wbkcjw wbk bwkbefj wkfh bfwkfb kefb nlwefl wkehf ewfnkj dcbb ', 'data' => '22.07.2022'],
        ['id' => 2, 'img'=>'https://cdn-icons-png.flaticon.com/512/3657/3657284.png','title' => 'News team', 'description' => 'This news about team cbibw wbckew wkbvc wbkcjw wbk bwkbefj wkfh bfwkfb kefb nlwefl wkehf ewfnkj dcbb ', 'data' => '18.11.2022']
    ];
    return view('new', ['news' => $news[$newsId]]); 
});
Route::get('/about', function () {
    return view('about');
});