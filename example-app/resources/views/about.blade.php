@include('templates.header')
    <main class="page">
        <section class="page__home home" id="about">
            <div class="home__conteiner conteiner">
                <div class="home__card card">
                    <h1 class="card__title">About Us</h1>
                    <p class="card__description" >When you make investments, you have the potential to make money (called a return). Money in a bank account pays interest, which is your return. You earn that small amount of interest for allowing the bank to keep your money. The bank then turns around and lends your money to some other person or organization at a much higher rate of interest.</p>
                    <div>
                        <img class="card__img" src="https://psv4.userapi.com/c534536/u226801185/docs/d30/bc2ee755cbab/Group_281.png?extra=vDjPrp53-LhS1kudSLCpLSiEu3KiN8fSwDagaqpt8jT7wCGZi0qA1bljmLivSKwprfko3L_kbdZiLEdtz2b-cOjNqs5r7edCSU_8_6eUuoYYzBhiVqYFt_bmxUKBTWug894WssPnQh1lo-aZt1l6-s0NNac" alt="img">
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
@include('templates.footer')
<style>
.home{
    margin-top: 30px; width: 100% 
} 
.conteiner{
    max-width: 1400px;
    padding: 0px 80px;
    margin: 0px auto;
    box-sizing: content-box;
}
.card{
    background: -webkit-linear-gradient(67deg, #7e00e6 0%,#d2b7e1 60%, #c4a0ee 80%); opacity: 0.7;       
    width: 1100px;
    height: 600px;
    margin: 0 auto;
    border-radius: 20px
}
.card__title{
    color: #ffff;
    margin-left: 50px;
    padding-top:110px;
    font-size: 42px;
    width: 550px;
}
.card__description{
    color: #ffff;
    margin-top: 20px;
    margin-left: 50px;
    font-weight:600;
    width: 500px;
    font-size: 18px
}
.card__button{
    color: #ffff;
    font-weight:600;
    margin-top: 15px;
    margin-left: 50px;
    padding: 15px 45px;
    background: linear-gradient(89.77deg, #4B0082 0.2%, #9370DB 99.82%);
    box-shadow: 0px 10px 20px rgba(147, 112, 219, 0.2);
    border-radius: 25px;
}
.card__img{
    width: 42%;
    margin-left: 600px;
    margin-top: -450px;

}
</style>

