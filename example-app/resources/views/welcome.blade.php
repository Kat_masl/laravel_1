@include('templates.header')
    <main class="page">
        <section class="page__home home" id="home">
            <div class="home__conteiner conteiner">
                <div class="home__card card">
                    <h1 class="card__title">Application design project for novice investors</h1>
                    <p class="card__description" >Hello, on this page you will see the project design of the application for investment</p>
                    <button class="card__button button">
                        <a href="#" class="button__link">Order project</a>
                    </button>
                    <div>
                    <img class="card__img" src="https://psv4.userapi.com/c235131/u226801185/docs/d51/613b34f2d90d/Group.png?extra=MScnkRfGwvxFOoRs80diEmEYtDbsmlmH7Zz3sT10wx7fV9018bCMnpze0Aat0f_v5PRwMtPff85m5WirCtRQtKl6sMT9Q0ojWffaXq4hbJ74akjhDH16FxYoF295GO4Ufad2-blJEbtX2bTXXQ8PLqeKCg" alt="img">
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
@include('templates.footer')
<style>
.home{
    margin-top: 30px; width: 100% 
} 
.conteiner{
    max-width: 1400px;
    padding: 0px 80px;
    margin: 0px auto;
    box-sizing: content-box;
}
.card{
    background: -webkit-linear-gradient(67deg, #c4a0ee 0%,#d2b7e1 10%,#7e00e6 60%); opacity: 0.7;       
    width: 1100px;
    height: 600px;
    margin: 0 auto;
    border-radius: 20px
}
.card__title{
    color: #ffff;
    margin-left: 50px;
    padding-top:110px;
    font-size: 42px;
    width: 550px;
}
.card__description{
    color: #ffff;
    margin-top: 20px;
    margin-left: 50px;
    font-weight:600;
    width: 500px;
    font-size: 18px
}
.card__button{
    color: #ffff;
    font-weight:600;
    margin-top: 15px;
    margin-left: 50px;
    padding: 15px 45px;
    background: linear-gradient(89.77deg, #4B0082 0.2%, #9370DB 99.82%);
    box-shadow: 0px 10px 20px rgba(147, 112, 219, 0.2);
    border-radius: 25px;
}
.card__img{
    width: 42%;
    margin-left: 600px;
    margin-top: -475px;

}
</style>

