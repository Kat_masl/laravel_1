@include('templates.header')
    <main class="page">
        <section class="catalogs"> <!--Women's fashion-->
            <div class="catalogs__wrapper">
                <div class="catalogs__product product">
                    <div class="product__product-card product-card">
                        <img class="product-card__img" src="{{$news['img']}}" alt="picture 6">
                        <h2 class="product-card__biz">{{$news['title']}}</h2>
                        <p class="product-card__disc">{{$news['description']}}</p>
                        <p class="product-card__data">1{{$news['data']}}</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
@include('templates.footer')
<style>
.catalogs{
  display: flex;
  margin-top: 10px;
  min-width: 1138px;
  min-height: 617px;
}
.catalogs__wrapper{
  margin-left: 600px;
}
.catalogs__heading {
  color: #ffff;
  margin-left: 480px;
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  text-transform: uppercase;
}

.product {
  display: flex;
  margin-top: 65px;
}

.product-card {
  margin: 0px 22px 0px 0px;
}

.product-card__img {
    width: 40%;
}

.product-card__biz {
  margin-top: 38px;
  color: #ffff;
  font-weight: 300;
  font-size: 15px;
  line-height: 22.5px;
  text-transform: uppercase;
}

.product-card__disc{
  width: 400px;
  margin-top: 15px;
  color: #ffff;
  font-weight: 300;
  font-size: 10px;
  line-height: 22.5px;
  text-transform: uppercase;  
}

.product-card__data{
  margin-top: 10px;
  color: #ffff;
  font-weight: 200;
  font-size: 13px;
  line-height: 22.5px;
}

</style>

