@include('templates.header')
    <main class="page">
        <section class="catalogs"> <!--Women's fashion-->
            <div class="catalogs__wrapper">
                <h2 class="catalogs__heading">News</h2>
                <div class="catalogs__product product">
                    <div class="product__product-card product-card">
                        <img class="product-card__img" src="https://cdn-icons-png.flaticon.com/512/325/325717.png" alt="picture 6">
                        <h2 class="product-card__biz">News project</h2>
                        <p class="product-card__disc">This news about project</p>
                        <p class="product-card__data">10.03.2022</p>
                        <button class="product-card__button button">
                            <a href="/news/0" class="button__link">Detail new</a>
                        </button>
                    </div>
                    <div class="product__product-card product-card">
                        <img class="product-card__img" src="https://cdn-icons-png.flaticon.com/512/325/325714.png" alt="picture 6">
                        <h2 class="product-card__biz">News design</h2>
                        <p class="product-card__disc">This news about design</p>
                        <p class="product-card__data">22.07.2022</p>
                        <button class="product-card__button button">
                            <a href="/news/1" class="button__link">Detail new</a>
                        </button>
                    </div>
                    <div class="product__product-card product-card">
                        <img class="product-card__img" src="https://cdn-icons-png.flaticon.com/512/3657/3657284.png" alt="picture 6">
                        <h2 class="product-card__biz">News team</h2>
                        <p class="product-card__disc">This news about team</p>
                        <p class="product-card__data">18.11.2022</p>
                        <button class="product-card__button button">
                            <a href="/news/2" class="button__link">Detail new</a>
                        </button>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
@include('templates.footer')
<style>
.catalogs{
  display: flex;
  margin-top: 10px;
  min-width: 1138px;
  min-height: 617px;
}
.catalogs__wrapper{
  margin-left: 200px;
}
.catalogs__heading {
  color: #ffff;
  margin-left: 480px;
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  text-transform: uppercase;
}

.product {
  display: flex;
  margin-top: 65px;
}

.product-card {
  margin: 0px 22px 0px 0px;
}

.product-card__img {
    width: 40%;
}

.product-card__biz {
  margin-top: 38px;
  color: #ffff;
  font-weight: 300;
  font-size: 15px;
  line-height: 22.5px;
  text-transform: uppercase;
}

.product-card__disc{
  margin-top: 15px;
  color: #ffff;
  font-weight: 300;
  font-size: 10px;
  line-height: 22.5px;
  text-transform: uppercase;  
}

.product-card__data{
  margin-top: 10px;
  color: #ffff;
  font-weight: 200;
  font-size: 13px;
  line-height: 22.5px;
}

.product-card__button{
    color: #ffff;
    font-weight:600;
    margin-top: 15px;
    padding: 10px 25px;
    background: linear-gradient(89.77deg, #4B0082 0.2%, #9370DB 99.82%);
    box-shadow: 0px 10px 20px rgba(147, 112, 219, 0.2);
    border-radius: 25px;
}
</style>

